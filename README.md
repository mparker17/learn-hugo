# mparker17/learn-hugo

Learning to use Hugo [using Derek's tutorial on consensus.enterprises](https://consensus.enterprises/blog/add-hugo-docs-to-gitlab-project/).

See the results at https://mparker17.gitlab.io/learn-hugo/
